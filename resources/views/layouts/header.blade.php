<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <!-- -------- -->
      <link href="https://vjs.zencdn.net/7.6.0/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>

<body>
    <div class="total">
        <div class="row form-group header">
            <div class="col-md-5"></div>
            <!-- ---------------------------------------------->
            <div class="row col-md-3 page">
                <div class="col-md-8"></div>
                <div class="page col-md-4">
                    <a href="{{ url('/')}}">Trang chủ</a>
                </div>

            </div>
            <!-- ------------------------------------------- -->
            <div class="col-md-2 listflim">
                <div class="col-md-2"></div>
                <div class="col-md-10"><a href="{{url('')}}">Danh sách phim</a></div>

            </div>
            <!-- ------------------------------------------- -->
            <div class="col-md-2 login"><a href="{{url('/login')}}">Đăng nhập</a></div>
        </div>
      <video id='my-video' class='video-js' controls preload='auto' width='640' height='264'
  poster='MY_VIDEO_POSTER.jpg' data-setup='{}'>
    <source src='https://mailserver01-my.sharepoint.com/personal/tuanpham0403_xmail_cloud/Documents/FILM/Bai%2045.%20Project%206%20Viet%20hieu%20ung%20modal%20box%20-%20phan%201%20html.mp4"
    type="video/mp4' type='video/mp4'>

  </video>
    </div>
</body>
  <script src='https://vjs.zencdn.net/7.6.0/video.js'></script>

</html>
